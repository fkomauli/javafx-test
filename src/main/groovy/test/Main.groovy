package test

import javafx.application.Application
import javafx.scene.*
import javafx.scene.layout.*
import javafx.stage.*

class Main extends Application {

    void start(Stage stage) {
        stage.title = 'GroovyFX Hello World'
        stage.scene = new Scene(root())
        stage.show()
    }

    private StackPane root() {
        return new StackPane()
    }

    static void main(String[] args) {
        launch(Main, args)
    }
}
