javapackager -deploy \
    -native deb \
    -name TestFX \
    -title 'Test FX App' \
    -vendor MetaHelicase \
    -outdir build/distributions/ \
    -appclass test.Main \
    -srcfiles build/libs/javafx-test-all.jar \
    -outfile TestFX \
    -BlicenseType=MIT \
    -Bcategory=Test
